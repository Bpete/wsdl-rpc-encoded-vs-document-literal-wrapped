<?php
/*
 * Sample Soap Service (WSDL Mode) *
 * By Brandon Peters (https://bitbucket.org/Bpete)
 * Last update ~ 10/20/2013
 */ 

ini_set("soap.wsdl_cache_enabled", 0);

//Begin Class
class SampleService {
	
	function getUserArray($fname,$lname,$email) { 
	
		$user_array = array();
		
		$user_array['first_name'] = $fname;
		$user_array['last_name'] = $lname;
		$user_array['email'] = $email;
		
		return $user_array;
		
	}
	
}//End Class

$server = new SoapServer("rpc-encode.wsdl");
$server->setClass('SampleService');
$server->handle();

?>