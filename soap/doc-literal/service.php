<?php
/*
 * Sample Soap Service (WSDL Mode) *
 * By Brandon Peters (https://bitbucket.org/Bpete)
 * Last update ~ 10/20/2013
 */ 

ini_set("soap.wsdl_cache_enabled", 0);

//Begin Class
class SampleService {
	
	function getUserArray($user) { 
	
		$user_array = array();
		
		$user_array['first_name'] = $user->fname;
		$user_array['last_name'] = $user->lname;
		$user_array['email'] = $user->email;
		
		return $user_array;
		
	}
	
}//End Class

$server = new SoapServer("doc-literal.wsdl");
$server->setClass('SampleService');
$server->handle();

?>